## pine-user 10 QKQ1.191014.001 V12.5.2.0.QCMMIXM release-keys
- Manufacturer: xiaomi
- Platform: msm8937
- Codename: pine
- Brand: Xiaomi
- Flavor: pine-user
- Release Version: 10
- Id: QKQ1.191014.001
- Incremental: V12.5.2.0.QCMMIXM
- Tags: release-keys
- CPU Abilist: armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 320
- Fingerprint: Xiaomi/pine/pine:10/QKQ1.191014.001/V12.5.2.0.QCMMIXM:user/release-keys
- OTA version: 
- Branch: pine-user-10-QKQ1.191014.001-V12.5.2.0.QCMMIXM-release-keys
- Repo: xiaomi_pine_dump_12851


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
